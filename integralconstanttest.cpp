#include <type_traits>
#include "simpletemplate.hpp"
using namespace ST;

template<typename T>
void force_unused(T&& t) {}

#define UNUSED(var) force_unused(var)

/*************************************************************************************************************/
/* construction, destruction, underlying_type, conversion */
template<typename T, T value>
void basic_test()
{
    constexpr IntegralConstant<T, value> i{};
    static_assert(std::is_same<typename decltype(i)::underlying_type, T>::value, "");

    //runtime construction and destruction
    {
        IntegralConstant<T, value> i2;
        UNUSED(i2);
    }

    //explicit conversion to T
    constexpr T t = static_cast<T>(i);
    static_assert(t == value, "");

    //explicit bool conversion
    static_assert(i || true, "must compile");

    //explicit conversion to non-cv T
    static_assert(static_cast<std::remove_cv_t<T>>(i) == value, "");
}

template<typename T>
void basic_test_for_type()
{
    basic_test<T, 0>();
    basic_test<T, std::numeric_limits<T>::min()>();
    basic_test<T, std::numeric_limits<T>::max()>();
}

/*************************************************************************************************************/
/* test all combination of types: signed / unsigned / const */

template<typename T>
void test_all()
{
    basic_test_for_type<T>();
}

template<typename T>
void test_all_sign()
{
    test_all<T>();
    test_all<std::make_signed_t<T>>();
    test_all<std::make_unsigned_t<T>>();
}

template<typename PlainType/*The type without signed / unsigned / const*/>
void test_all_sign_cv()
{
    test_all_sign<PlainType>();
    test_all_sign<std::add_const_t<PlainType>>();
}

template void test_all<bool>();
template void test_all<const bool>();

//problem: integralconstant bool shouldn't be specialization, instead we should define templated conversion operators
//template void test_all<volatile bool>();
//template void test_all<const volatile bool>();
template void test_all_sign_cv<char>();
template void test_all_sign_cv<char16_t>();
template void test_all_sign_cv<char32_t>();
template void test_all_sign_cv<wchar_t>();
template void test_all_sign_cv<short>();
template void test_all_sign_cv<int>();
template void test_all_sign_cv<long>();
template void test_all_sign_cv<long long>();
//TODO enum

//type promotion
template<typename T> constexpr void take_param(T) {}

template<typename FromType, typename ToType, long long value>
constexpr void implicit_promotion_test()
{
    using from_type = IntegralConstant<FromType, static_cast<FromType>(value)>;
    using to_type = IntegralConstant<ToType, static_cast<ToType>(value)>;
    using to_type_diff_value = IntegralConstant<ToType, static_cast<ToType>(value + 1)>;

    //this must compile
    take_param<to_type>(from_type{});

    static_assert(std::is_convertible<from_type, to_type>::value, "same values are convertible");
    static_assert(!std::is_convertible<from_type, to_type_diff_value>::value, "different values are not convertible");
}

template void implicit_promotion_test<bool, int, true>();
template void implicit_promotion_test<char, int, 'A'>();
template void implicit_promotion_test<int, long, 1290315123>();

//UDL precision
static_assert(std::is_same<decltype(0_c), IntegralConstant<std::int8_t, 0>>::value, "");
static_assert(std::is_same<decltype(1_c), IntegralConstant<std::int8_t, 1>>::value, "");
static_assert(std::is_same<decltype(127_c), IntegralConstant<std::int8_t, 127>>::value, "");
static_assert(std::is_same<decltype(128_c), IntegralConstant<std::int16_t, 128>>::value, "");
static_assert(std::is_same<decltype(32767_c), IntegralConstant<std::int16_t, 32767>>::value, "");
static_assert(std::is_same<decltype(32768_c), IntegralConstant<std::int32_t, 32768>>::value, "");
static_assert(std::is_same<decltype(2147483647_c), IntegralConstant<std::int32_t, 2147483647>>::value, "");
static_assert(std::is_same<decltype(2147483648_c), IntegralConstant<std::int64_t, 2147483648>>::value, "");
static_assert(std::is_same<decltype(9'223'372'036'854'775'807_c), IntegralConstant<std::int64_t, 9'223'372'036'854'775'807>>::value, "");

//UDL hex
static_assert((int)0x1_c == 0x1, "");
static_assert((int)0x10_c == 0x10, "");
static_assert((int)0x32_c == 0x32, "");
static_assert((int)0x19873495_c == 0x19873495, "");

//UDL oct
static_assert((int)02346360_c == 02346360, "");

//UDL bin
static_assert((int)0b10011_c == 0b10011, "");

//UDL separator compatibility
static_assert(static_cast<int>(1'234'5'67_c) == 1234567, "");
static_assert(static_cast<int>(0x1'234'5'67_c) == 0x1234567, "");
static_assert(static_cast<int>(01'234'5'67_c) == 01234567, "");
static_assert(static_cast<int>(0b10'101'100_c) == 0b10101100, "");

//arithmetic
static_assert(-99999_c == IntegralConstant<int, -99999>{}, "");
static_assert(+99999_c == IntegralConstant<int, 99999>{}, "");
static_assert(4_c + 3_c == 7_c, "");
static_assert(127_c + 2_c == 129_c, "");
static_assert(129_c - 2_c == 127_c, "");

//explicit bool should not be ambiguous
namespace ExplicitBoolTest
{
bool explicit_bool_from_bool = static_cast<bool>(true_c);
bool explicit_bool_from_int = static_cast<bool>(123456_c);
}