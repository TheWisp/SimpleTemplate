#pragma once

//Helper for applying static tester (containing only static asserts) to multiple types
template<template<typename> class Tester, typename ... Ts>
struct StaticVariadicTestSuite;

template<template<typename> class Tester>
struct StaticVariadicTestSuite<Tester> {};

template<template<typename> class Tester, typename T, typename ... Ts>
struct StaticVariadicTestSuite<Tester, T, Ts...>
{
	//force instantiate
	constexpr StaticVariadicTestSuite()
	{
		Tester<T>{}();
	}
	static constexpr auto remaining = StaticVariadicTestSuite<Tester, Ts...>{};
};


