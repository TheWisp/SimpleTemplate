###### Goals

Minimal: single header, minimum learning cost, one-obvious-way idiom
	ANYTHING THAT WE DONT NECESSARILY NEED, WE DONT NEED IT
	minimize both the amount and complexity of involved concepts and types
User-friendly: provide easy-to-use type transformation and dispatching API
	support overloaded API for type tags and user values: is_enum(typetag<E>) and is_enum(e) //e is E
	provide type-based API, e.g. is_enum_type<E>
SFINAE-friendly: avoid hard compilation errors for types (but do error for wrong usage or category errors)
C++14 support: gcc, clang, msvc
Fully compile time: otherwise why not just RTTI
Compile time type name (optional, only when wanted)
Value creation, e.g. TypeTag<T> ==> T(params)

###### Optional Goals:

C++17 support:
	do not drop C++14 compatibility
	Class template argument deduction: TypeTag t = ... instead of auto!
	template<auto> for integral constants
	N4268 "Allow constant evaluation for all non-type template arguments"
	deduction guide

C++11 support:
	variable template - unable to use typetag<> / typetaglist<> / boolconstant<>
	no generic lambda
	auto return type without trailing
	inline namespace (affects UDL)

Type-erased type tags

###### What can we provide besides type_traits
function traits: extract or synthesize function types, alter types and modifiers of function components (return type, parameter types, immutable type, ref overloading, [calling convention])

http://www.boost.org/doc/libs/develop/libs/callable_traits/doc/html/index.html

###### Possible improvements from C++17:

template<auto> for integral constants, i.e. constant<3> and constant<true> instead of intconstant<3> and boolconstant<true>
Class template argument deduction: TypeTag t = ... instead of auto!
constexpr-if instead of pretty much all overloading-based branches
constexpr lambda API wrapped in constexpr object to support both functional and template syntax,
e.g. is_integral(typetag<T>) and is_integral<T> would both return BoolConstant


###### Known bugs:

