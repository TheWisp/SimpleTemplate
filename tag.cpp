#include "simpletemplate.hpp"
#include <type_traits>
#include <vector>
#include "static_test_suite.h"
using namespace ST;

namespace InstantiationTest
{
	template<typename T>
	void test()
	{
		constexpr auto t = tag<T>;
		auto t_nonconstexpr = tag<T>;
		static_assert(std::is_same<T, TOTYPE(t)>::value, "tag<T> is the wrapped constant of type T");
	}

	template void test<void>();
	template void test<int>();
	template void test<bool>();
	template void test<char>();
	template void test<wchar_t>();
	template void test<float>();
	template void test<double>();
	template void test<void*>();
	template void test<int[]>();
	template void test<int[][5]>();
	template void test<void*[2]>();
	template void test<void()>();
	template void test<void()const>();
	template void test<bool(int*, long double)>();
	template void test<bool(*)(int*, long double)>();
	template void test<bool(&)(int*, long double)>();

	struct Foo {
		int f(double);
		int g(double) const;
	};
	template void test<Foo>();
	template void test<decltype(&Foo::f)>();
	template void test<decltype(&Foo::g)>();
	auto L = [x = 3](int, char){};
	template void test<decltype(L)>();
}

namespace ComparisonTest
{
	static_assert(tag<int> == tag<int>, "Equality");
	static_assert(tag<int> != tag<int&>, "Strict equality");
	static_assert(tag<int> != tag<const int>, "Strict equality");
	static_assert(tag<int> != tag<volatile int>, "Strict equality");
	static_assert(tag<int&> != tag<int&&>, "Strict equality");

	int x;
	int& y = x;
	int z = y;

	static_assert(tag<decltype(x)> != tag<decltype(y)>, "not same");
	static_assert(tag<decltype((x))> == tag<decltype(y)>, "expr");
	static_assert(tag<decltype(y)> == tag<int&>, "sdf");
	static_assert(tag<decltype(x)> == tag<decltype(z)>, "same");
}

//Common types are a collection of non-function, non-reference types without CV-qualifiers 
#define COMMON_TYPE_LIST	\
bool,						\
signed char,				\
unsigned char,				\
wchar_t,					\
char16_t,					\
char32_t,					\
signed short,				\
unsigned short,				\
signed int,					\
unsigned int,				\
signed long,				\
unsigned long,				\
signed long long,			\
unsigned long long,			\
float,						\
double,						\
long double,				\
void*, 						\
const char*, 				\
char[], 					\
int[][5]

namespace TransformationTest
{
	template<typename T>
	struct LValueRefTest
	{
		static_assert(!std::is_const<T>::value && !std::is_volatile<T>::value && !std::is_reference<T>::value,
			"Sanity: please make sure the base type argument is non-const, non-volatile, non-reference");

		constexpr void operator()()
		{
			//T + ref == T&
			static_assert(tag<T> +lvalue_reference_tag == tag<T&>, "");
			//T& - ref == T
			static_assert(tag<T&> -lvalue_reference_tag == tag<T>, "");
			//T + ref - ref == T
			static_assert(tag<T> +lvalue_reference_tag - lvalue_reference_tag == tag<T>, "");
			//T + const + ref == const T&
			static_assert(tag<T> +const_qualifier_tag + lvalue_reference_tag == tag<const T&>, "");
			//T + ref + const == const T&
			static_assert(tag<T> +lvalue_reference_tag + const_qualifier_tag == tag<const T&>, "");
		}
	};

	constexpr StaticVariadicTestSuite<LValueRefTest, COMMON_TYPE_LIST> lvalue_ref_test;

	template<typename T/*intended to be without const / volatile / reference*/>
	void type_modifier_test()
	{

		static_assert(!std::is_const<T>::value && !std::is_volatile<T>::value && !std::is_reference<T>::value,
			"Sanity: please make sure the test function argument is non-const, non-volatile, non-reference");

		constexpr auto T0 = tag<T>;
		constexpr auto T1 = T0 - lvalue_reference_tag;
		constexpr auto T2 = T0 + rvalue_reference_tag;
		constexpr auto T3 = T0 + lvalue_reference_tag;
		constexpr auto T4 = T3 - reference_tag;
		constexpr auto T5 = T2 + const_qualifier_tag - reference_tag + lvalue_reference_tag;
		static_assert(T5 == tag<const T&>, "");

		static_assert(tag<T> +const_qualifier_tag == tag<const T>, "");
		static_assert(tag<T> +const_qualifier_tag != tag<T>, "");
		static_assert(tag<T> +const_qualifier_tag + lvalue_reference_tag == tag<const T&>, "");

		static_assert(tag<T> +rvalue_reference_tag == tag<T&&>, "");
		static_assert(tag<T> +lvalue_reference_tag == tag<T&>, "");
		static_assert(tag<T> +lvalue_reference_tag + rvalue_reference_tag == tag<T> +lvalue_reference_tag, "");
		static_assert(tag<T> +rvalue_reference_tag + lvalue_reference_tag == tag<T> +lvalue_reference_tag, "");
		static_assert(tag<T> +const_qualifier_tag + lvalue_reference_tag == tag<const T&>, "");
		static_assert(tag<T> +lvalue_reference_tag + const_qualifier_tag == tag<const T&>, "");

		static_assert(T2 + const_qualifier_tag == tag<const T&&>, "");
		static_assert(T2 + const_qualifier_tag - const_qualifier_tag == tag<T&&>, "");

		static_assert(tag<T&> +const_qualifier_tag == tag<const T&>, "");
		static_assert(tag<T&> +const_qualifier_tag - reference_tag == tag<const T>, "");
		static_assert(tag<T&> -reference_tag + const_qualifier_tag == tag<const T>, "");

		static_assert(tag<T> +pointer_tag == tag<T*>, "");
		static_assert(tag<T*> -pointer_tag == tag<T>, "");
	}

	template void type_modifier_test<int>();
	template void type_modifier_test<bool>();
	template void type_modifier_test<char>();
	template void type_modifier_test<const char*>();
	template void type_modifier_test<char[]>();

	//function <=> function reference
	static_assert(tag<int(double)> +lvalue_reference_tag == tag<int(&)(double)>, "");
	static_assert(tag<int(&)(double)> -lvalue_reference_tag == tag<int(double)>, "");
}

namespace CategoryTest
{
	enum class TestResult
	{
		Void,
		Nullptr,
		Integral,
		FloatingPoint,
		Array,
		Enum,
		Union,
		Class,
		Function,
		Pointer,
		LValueReference,
		RValueReference,
		PointerToMemberObject,
		PointerToMemberFunction
	};

	constexpr TestResult test_type_category_func(VoidTag) { return TestResult::Void; }
	constexpr TestResult test_type_category_func(NullptrTag) { return TestResult::Nullptr; }
	constexpr TestResult test_type_category_func(IntegralTag) { return TestResult::Integral; }
	constexpr TestResult test_type_category_func(FloatingPointTag) { return TestResult::FloatingPoint; }
	constexpr TestResult test_type_category_func(ArrayTag) { return TestResult::Array; }
	constexpr TestResult test_type_category_func(EnumTag) { return TestResult::Enum; }
	constexpr TestResult test_type_category_func(UnionTag) { return TestResult::Union; }
	constexpr TestResult test_type_category_func(ClassTag) { return TestResult::Class; }
	constexpr TestResult test_type_category_func(FunctionTag) { return TestResult::Function; }
	constexpr TestResult test_type_category_func(PointerTag) { return TestResult::Pointer; }
	constexpr TestResult test_type_category_func(LValueReferenceTag) { return TestResult::LValueReference; }
	constexpr TestResult test_type_category_func(RValueReferenceTag) { return TestResult::RValueReference; }
	constexpr TestResult test_type_category_func(PointerToMemberFunctionTag) { return TestResult::PointerToMemberFunction; }
	constexpr TestResult test_type_category_func(PointerToMemberObjectTag) { return TestResult::PointerToMemberObject; }

	static_assert(test_type_category_func(tag<void>.category()) == TestResult::Void, "");
	static_assert(test_type_category_func(tag<std::nullptr_t>.category()) == TestResult::Nullptr, "");
	static_assert(test_type_category_func(tag<long long>.category()) == TestResult::Integral, "");
	static_assert(test_type_category_func(tag<float>.category()) == TestResult::FloatingPoint, "");
	static_assert(test_type_category_func(tag<const char[10]>.category()) == TestResult::Array, "");
	static_assert(test_type_category_func(tag<TestResult>.category()) == TestResult::Enum, "");
	union U {};
	static_assert(test_type_category_func(tag<U>.category()) == TestResult::Union, "");
	static_assert(test_type_category_func(tag<std::vector<int>>.category()) == TestResult::Class, "");
	void F() {};
	static_assert(test_type_category_func(tag<decltype(F)>.category()) == TestResult::Function, "");
	static_assert(test_type_category_func(tag<decltype(&F)>.category()) == TestResult::Pointer, "");

	static_assert(test_type_category_func(tag<int&&>.category()) == TestResult::RValueReference, "");

	struct C {
		float x;
		int F() { return 0; }
	};
	static_assert(test_type_category_func(tag<decltype(&C::x)>.category()) == TestResult::PointerToMemberObject, "");
	static_assert(test_type_category_func(tag<decltype(&C::F)>.category()) == TestResult::PointerToMemberFunction, "");

	static_assert(test_type_category_func(tag<const int&>.category()) == TestResult::LValueReference, "");
	static_assert(test_type_category_func((tag<const int&> -reference_tag).category()) == TestResult::Integral, "");

}

