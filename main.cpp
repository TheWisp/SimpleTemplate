#include "simpletemplate.hpp"
#include <iostream>
#include <tuple>
#include <vector>
#include <deque>
#include <array>

template<typename T>
void force_unused(T&& t) {}

#define UNUSED(var) force_unused(var)

using ST::operator""_c;

namespace ST
{
/*************************************************************************************************************/
/* Integral constant tests */
static_assert(true_c == true_c, "comparison");
static_assert(true_c != false_c, "comparison");
static_assert(false_c == false_c, "comparison");
static_assert(false_c != true_c, "comparison");
static_assert(true_c, "implicit explicit bool");
static_assert(!false_c, "implicit explicit bool");
static_assert(true_c ? true : false, "implicit explicit bool");
static_assert(false_c ? false : true, "implicit explicit bool");
static_assert((bool)true_c == true, "explicit bool");
static_assert(true == (bool)true_c, "explicit bool");
static_assert((bool)false_c == false, "explicit bool");
static_assert(false == (bool)false_c, "explicit bool");

static_assert(tag<int>.size() == sizeof_type<int>(), "");
static_assert(tag<int>.size() == 4_c, "");
static_assert(tag<int>.size(), "");

static_assert(countoftypes<int, float, void> == 3_c, "");
template<typename... Ts>
struct VariadicClass
{
    static constexpr auto typecount = countoftypes<Ts...>;
    static constexpr auto typecount_normal = sizeof...(Ts);
};
constexpr bool overloaded_func(IntegralConstant<size_t, 4>) { return true; }
constexpr bool overloaded_func(...) { return false; }
static_assert(overloaded_func(VariadicClass<int, int&, void, bool>::typecount), "integral constant");
static_assert(!overloaded_func(VariadicClass<int, int&, void, bool>::typecount_normal), "just size_t value");

/*************************************************************************************************************/
/* type tag creation tests*/
namespace TestTypeTagCreation
{
	constexpr auto tvoid = tag<void>;
	constexpr auto tf1 = tag<void(int)>;
}

/*************************************************************************************************************/
/* type property tests */

static_assert(tag<int[]>.category() == array_tag, "");
static_assert(tag<int[3]>.category() == array_tag, "");
static_assert(tag<int>.category() != array_tag, "");

constexpr auto t1 = tag<int[3]>;
static_assert(t1.size() == sizeof_type<int>() * 3_c, "");

static_assert(tag<int>.category() == integral_tag, "");
static_assert(tag<void(int)>.return_type() == tag<void>, "");

/*************************************************************************************************************/
static_assert(list<> == list<>, "");
static_assert(list<int> != list<>, "");
static_assert(list<> != list<void>, "");
static_assert(list<void> == list<void>, "");
static_assert(list<void> -tag<void> == list<>, "");
static_assert(list<void> -tag<void> +tag<int> == list<int>, "");

//type of comparison result is BoolConstant
static_assert((list<> == list<>) == true_c, "");
static_assert((list<> != list<>) == false_c, "");
static_assert((list<void> != list<>) == true_c, "");
static_assert((list<void> == list<>) == false_c, "");

constexpr auto li = list<int, float, bool>;
static_assert(li[0_c] == tag<int>, "");
constexpr auto idx_0 = 0_c;
static_assert(li[idx_0] == tag<int>, "");
static_assert(li + tag<void> == list<int, float, bool, void>, "");
static_assert(li - tag<float> == list<int, bool>, "");
static_assert(tag<void> +li == list<void, int, float, bool>, "");
static_assert(list<void, int> +tag<void> +list<bool, const char*> == list<void, int, void, bool, const char*>, "");
static_assert(li + tag<const float> == list<int, float, bool, const float>, "");
static_assert(list<int, float, bool, const float> -tag<const float> == list<int, float, bool>, "qualifier sensitive");

//'-' removes first match
static_assert(list<int, bool, void, float, char, float> -tag<float> == list<int, bool, void, char, float>, "");
static_assert(list<int, float> -tag<char> == list<int, float>, "");

//length
static_assert(list<int, float>.length == 2_c, "");
static_assert(list<>.length == 0_c, "");

//reverse
static_assert(reverse(list<int, bool, void>) == list<void, bool, int>, "");

//combine
static_assert(combine(partialtypetag<std::tuple>, list<int, float>) == tag<std::tuple<int, float>>, "");
static_assert(combine(partialtypetag<std::tuple>, tag<int>) == tag<std::tuple<int>>, "");
static_assert(combine(partialtypetag<std::tuple>, list<int> +tag<const char*>) == tag<std::tuple<int, const char*>>, "");
static_assert(combine(partialtypetag<std::vector>, tag<bool>) == tag<std::vector<bool>>, "");

/*************************************************************************************************************/

static_assert(type_category<void>() == VoidTag{}, "");
static_assert(type_category<const int>() == IntegralTag{}, "");

namespace TypeDispatchingExample
{
	template<typename T>
	constexpr int generic_func_example_impl(T&& t, IntegralTag) { return 1; }

	template<typename T>
	constexpr int generic_func_example_impl(T&& t, EnumTag) { return 2; }

	constexpr int generic_func_example_impl(...) { return -1; }

	template<typename T>
	constexpr int generic_func_example(T&& t)
	{
		return generic_func_example_impl(t, type_category<T>());
	}

	static_assert(generic_func_example(32) == 1, "");

	enum class E {A};
	static_assert(generic_func_example(E::A) == 2, "");
	static_assert(generic_func_example(3.f) == -1, "");

	template<typename T, typename = TypeCategory<T>>
	struct GenericClassExample {
		static constexpr int x = 0;
	};

	template<typename T>
	struct GenericClassExample<T, EnumTag> {
		static constexpr int x = 1;
	};

	template<typename T>
	struct GenericClassExample<T, FloatingPointTag> {
		static constexpr int x = 2;
	};

	template<typename T>
	struct GenericClassExample<T, PointerTag> {
		static constexpr int x = 3;
	};

	template<typename T>
	struct GenericClassExample<T, LValueReferenceTag> {
		static constexpr int x = 4;
	};

	static_assert(GenericClassExample<int>::x == 0, "");
	static_assert(GenericClassExample<double>::x == 2, "");
	static_assert(GenericClassExample<int&>::x == 4, "");
}

}

template<typename T>
class MyStorage
{
public:

    static constexpr auto element_tag = ST::tag<T>;

    static constexpr auto storage_tag = select(
        (ST::sizeof_type<T>() >= 8_c),
        ST::partialtypetag<std::deque>,
        ST::partialtypetag<std::vector>
    );

    static constexpr auto allocator_tag = select(
        element_tag.category() == ST::integral_tag,
        ST::tag<std::allocator<int[16]>>,
        ST::tag<std::allocator<T>>
    );

	using storage_type = TOTYPE(combine(storage_tag, element_tag, allocator_tag));

    storage_type container;
};

void test_my_storage()
{
    MyStorage<int> mystorage1;
    using storagetype1 = decltype(mystorage1)::storage_type;

    static_assert(ST::tag<storagetype1> == ST::tag<std::vector<int, std::allocator<int[16]>>>, "");
    struct Foo
    {
        std::array<int, 100> arr;
    };

    MyStorage<Foo> mystorage2;
    using storagetype2 = decltype(mystorage2)::storage_type;
    static_assert(ST::tag<storagetype2> == ST::tag<std::deque<Foo, std::allocator<Foo>>>, "");
}

enum class E : short
{
    a = 0x0000,
    b,
    c,
    d
};

template<
	typename T, 
	typename = decltype(ST::tag<T>.category()), 
	typename = decltype((ST::tag<T> -ST::reference_tag).category())>
class MySpecializationClassTest
{
public:
	static constexpr bool is_base_template = true;
};

template<typename T>
class MySpecializationClassTest<T, ST::LValueReferenceTag, ST::ClassTag>
{
public:
	static constexpr bool is_base_template = false;
};

int main()
{
    static_assert(ST::tag<int>.underlying_type() == ST::none, "");
    static_assert(ST::tag<E>.underlying_type() == ST::tag<short>, "");

    static_assert(ST::tag<const std::vector<int>&>.category() == ST::reference_tag, "");
    static_assert(MySpecializationClassTest<int>::is_base_template == true, "");
    static_assert(MySpecializationClassTest<int&>::is_base_template == true, "");
    static_assert(MySpecializationClassTest<const std::vector<int>>::is_base_template == true, "");

	static_assert(ST::tag<const std::vector<int>&>.category() == ST::reference_tag, ":");
    static_assert(MySpecializationClassTest<const std::vector<int>&>::is_base_template == false, "");

    std::cin.get();
}